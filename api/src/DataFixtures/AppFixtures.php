<?php

namespace App\DataFixtures;

use App\Entity\Candidature;
use App\Entity\Offer;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\Date;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * AppFixtures constructor.
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        // Users
        $adminUser = (new User())
            ->setEmail('admin@admin.com')
            ->setRoles([User::ROLE_ADMIN_NAME, User::ROLE_CANDIDATE_NAME, User::ROLE_RECRUITER_NAME])
            ->setFirstname('Admin')
            ->setLastname('admin')
            ->setGender('male')
            ->setAge(20)
            ->setAddress('10 rue de Paris, 75000 Paris');
        $adminUser->setPassword($this->passwordEncoder->encodePassword($adminUser, 'admin'));
        $manager->persist($adminUser);
        $recruiterUser1 = (new User())
            ->setEmail('recruiter1@recruiter.com')
            ->setRoles(['ROLE_RECRUITER'])
            ->setFirstname('Recruiter')
            ->setLastname('user')
            ->setGender('male')
            ->setAge(20)
            ->setAddress('10 rue de Paris, 75000 Paris');
        $recruiterUser1->setPassword($this->passwordEncoder->encodePassword($recruiterUser1, 'recruiter'));
        $manager->persist($recruiterUser1);
        $recruiterUser2 = (new User())
            ->setEmail('recruiter2@recruiter.com')
            ->setRoles(['ROLE_RECRUITER'])
            ->setFirstname('Recruiter2')
            ->setLastname('user')
            ->setGender('male')
            ->setAge(20)
            ->setAddress('10 rue de Paris, 75000 Paris');
        $recruiterUser2->setPassword($this->passwordEncoder->encodePassword($recruiterUser2, 'recruiter'));
        $manager->persist($recruiterUser2);
        $candidateUser1 = (new User())
            ->setEmail('candidate1@candidate.com')
            ->setRoles(['ROLE_CANDIDATE'])
            ->setFirstname('Candidate')
            ->setLastname('user')
            ->setGender('male')
            ->setAge(20)
            ->setAddress('10 rue de Paris, 75000 Paris');
        $candidateUser1->setPassword($this->passwordEncoder->encodePassword($candidateUser1, 'candidate'));
        $manager->persist($candidateUser1);
        $candidateUser2 = (new User())
            ->setEmail('candidate2@candidate.com')
            ->setRoles(['ROLE_CANDIDATE'])
            ->setFirstname('Candidate2')
            ->setLastname('user')
            ->setGender('male')
            ->setAge(20)
            ->setAddress('10 rue de Paris, 75000 Paris');
        $candidateUser2->setPassword($this->passwordEncoder->encodePassword($candidateUser2, 'candidate'));
        $manager->persist($candidateUser2);

        // Offers
        $offer1 = (new Offer())
            ->setName('Full stack php developper - London')
            ->setCompanyDescription('Join the IBIL Team in London')
            ->setOfferDescription('Full stack php developper')
            ->setStartDate(new \DateTime('2020-12-12'))
            ->setContractType('CDI')
            ->setWorkplace('London - 12 row alley')
            ->setRecruiter($recruiterUser1);
        $manager->persist($offer1);
        $offer2 = (new Offer())
            ->setName('Integrateur web - Paris')
            ->setCompanyDescription('Rejoignez l\'agence BB à Paris')
            ->setOfferDescription('Intégrateur web')
            ->setStartDate(new \DateTime('2020-12-12'))
            ->setContractType('CDD')
            ->setWorkplace('Paris - 12ème')
            ->setRecruiter($recruiterUser2);
        $manager->persist($offer2);

        // Candidatures
        $candidature1 = (new Candidature())
            ->setMotivationText('Je suis très motivé')
            ->setSalaryClaim(40000)
            ->setResume('resume')
            ->setStatus('pending')
            ->setCandidate($candidateUser1)
            ->setOffer($offer1);
        $manager->persist($candidature1);
        $candidature2 = (new Candidature())
            ->setMotivationText('Je suis encore plus motivé')
            ->setSalaryClaim(30000)
            ->setResume('resume')
            ->setStatus('pending')
            ->setCandidate($candidateUser2)
            ->setOffer($offer1);
        $manager->persist($candidature2);
        $candidature3 = (new Candidature())
            ->setMotivationText('Je DE OUF motivé')
            ->setSalaryClaim(55000)
            ->setResume('resume')
            ->setStatus('pending')
            ->setCandidate($candidateUser1)
            ->setOffer($offer2);
        $manager->persist($candidature3);

        $manager->flush();
    }
}
